package com.trnql.divamgupta.runwithfriends;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.trnql.smart.base.SmartCompatActivity;
import com.trnql.smart.location.LocationEntry;
import com.trnql.smart.location.LocationHistory;
import com.trnql.smart.people.PersonEntry;
import com.trnql.smart.weather.WeatherEntry;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;


public class MainActivity extends SmartCompatActivity
        implements NavigationDrawerCallbacks {

    Fragment fragment = null;

    private WeatherEntry weather ;

    private TextView currentConditions;
    private TextView highLow;
    private TextView feelsLike;
    private TextView forecast;
    private TextView rain;
    private TextView wind;
    private TextView uvIndex;
    private TextView humidity;
    private TextView sunrise;
    private TextView sunset;
    private TextView learnMoreText;

//    TextView mainT;


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        getAppData().setApiKey("e31d5c60-6c25-44c2-8fbe-88108d27b8dd");
        getAppData().startAllServices();
        getPeopleManager().setUserToken(AllData.fbId);
        getPeopleManager().setProductName("Run with Friends");
        try
        {
            AllData.gps_x = LocationHistory.getLastKnownLocation().getLatitude();
            AllData.gps_y = LocationHistory.getLastKnownLocation().getLongitude();
        }
        catch(Exception e) {}

//        Get current location

//        mainT = (TextView) findViewById(R.id.textView);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
    }

    @Override
    protected void smartWeatherChange(WeatherEntry weather) {
        this.weather = weather;

        Toast.makeText(this, "weather " + weather.toString() , Toast.LENGTH_SHORT).show();
        getPeopleManager().setDataPayload(AllData.name);

    }

    @Override
    protected void smartPeopleChange(List<PersonEntry> people) {
        String s = "";
        AllData.local_friends.clear();
        AllData.io.clear();

        for (PersonEntry person : people) {
            s += person.toString();
            AllData.io.add(person);
            OtherPeople o = new OtherPeople(person.getDataPayload().toString() , person.getLatitude(), person.getLongitude());
            o.mapStr = person.getDataPayload() + " - " + person.getActivityString();
//            o.speed = person.
            AllData.local_friends.put(person.getUserToken(),  o );


        }



//        Toast.makeText(this, "person yo  " + s, Toast.LENGTH_SHORT).show();
//        mainT.setText(s);

        createJSONList();

        try{
            ((HomeFragment) fragment).locationChanged();
        }catch (Exception e)
        {}


    }

    @Override
    protected void smartLatLngChange(LocationEntry location) {
        Toast.makeText(this, "my yo " + location.toString(), Toast.LENGTH_SHORT).show();
        getPeopleManager().setDataPayload(AllData.name);
        AllData.gps_x=location.getLatitude();
        AllData.gps_y=location.getLongitude();


        try{
            ((HomeFragment) fragment).locationChanged();
        }catch (Exception e)
        {}


        //
        // location.getSpeed()

//        if(location.a)
//        AllData.totalRun



    }

    private void displayView(int position)
    {

        switch (position) {
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new SecondFragment();
                break;
            case 2:
                fragment = new ChartFragment();
                break;
//            case 3:
//                fragment = new CommunityFragment();
//                break;
//            case 4:
//                fragment = new PagesFragment();
//                break;
//            case 5:
//                fragment = new WhatsHotFragment();
//                break;

            default:
                break;
        }


        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment).commit();

            // update selected item and title, then close the drawer
//            mDrawerList.setItemChecked(position, true);
//            mDrawerList.setSelection(position);
//            setTitle(navMenuTitles[position]);
//            mDrawerLayout.closeDrawer(mDrawerList);
        }

    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        Toast.makeText(this, "Menu item selected -> " + (position+1), Toast.LENGTH_SHORT).show();

        displayView( position);
    }


    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void createJSONList()
    {
        int i=0;
        PersonEntry temp;
        int is_friend = 0;
        String tempo;

        AllData.Jsonarr  = new JSONArray();


        for(i=0;i<AllData.io.size();++i)
        {
            try {
                temp = AllData.io.get(i);
                if (AllData.local_friends.containsKey(temp.getUserToken())) {
                    if (AllData.fb_friends.containsKey(temp.getUserToken())) {
                        is_friend = 1;
                    } else {
                        is_friend = 0;
                    }
                }
                JSONObject potato = new JSONObject();
//                Log.i("tomato",AllData.local_friends.get(temp.getUserToken()).name);
                potato.put("name",  temp.getDataPayload().replace("\n", ""));
                potato.put("distance",temp.getDistanceFromUser());
                potato.put("activity", temp.getActivityString().replace("\n", ""));
                potato.put("payload",temp.getDataPayload().replace("\n", ""));
                potato.put("id",temp.getUserToken().replace("\n", ""));
                potato.put("isFriend",is_friend);

                AllData.Jsonarr.put(potato);
            }
            catch(Exception e){}
        }
        for(i=0;i<AllData.Jsonarr.length();++i)
        {
            try
            {
                System.out.println(AllData.Jsonarr.get(i).toString());
                Log.i("potato", AllData.Jsonarr.get(i).toString());
            }
            catch(Exception e) {}
        }
    }


    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first

        //Write to localstorage
//        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("local_people","JSON String");
//        editor.commit();

    }

}
