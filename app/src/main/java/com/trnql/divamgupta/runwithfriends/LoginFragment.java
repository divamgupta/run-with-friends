package com.trnql.divamgupta.runwithfriends;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */


public class LoginFragment extends Fragment {

    private CallbackManager callbackManager;
    private TextView textView;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    private ProfileTracker mProfileTracker;

    Fragment that = this;

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            AccessToken accessToken = loginResult.getAccessToken();

           // Profile profile = Profile.getCurrentProfile();

           // displayMessage(profile);

            AllData.accessToken = accessToken.getToken();
            AllData.fbId = accessToken.getUserId();


            System.out.print(accessToken.toString());

            if(Profile.getCurrentProfile() == null) {
                mProfileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        Log.v("facebook - profile", profile2.getFirstName());
                        AllData.name = profile2.getName();
                        mProfileTracker.stopTracking();
                    }
                };
                mProfileTracker.startTracking();
            }
            else {
                Profile profile = Profile.getCurrentProfile();
                Log.v("facebook - profile", profile.getFirstName());
                AllData.name = profile.getName();
            }

            //

            Log.d("ff", "onSuces()" + AllData.toStringg());

            //Get friend list
            GraphRequest requestt =
                    new GraphRequest(
                            accessToken,
                            "/me/taggable_friends",
                            null,
                            HttpMethod.GET,
                            new GraphRequest.Callback() {
                                public void onCompleted(GraphResponse response) {
                                    JSONObject obj = response.getJSONObject();
                                    JSONArray arr;
                                    try {
                                        arr = obj.getJSONArray("data");
                                        int i=0;
                                        for(i=0;i<arr.length();++i) {
                                            //Get coordinates
                                            System.out.println("------"+arr.getJSONObject(i).get("name").toString()+"--------");
                                            AllData.fb_friends.put(arr.getJSONObject(i).get("id").toString(),new OtherPeople(arr.getJSONObject(i).get("name").toString(),1.0+i,1.0+i));
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                            }
                    );

            Bundle parameters = new Bundle();
            parameters.putString("fields", "name");
//            request.setParameters(parameters);
//            request.executeAsync();
            requestt.setParameters(parameters);
            requestt.executeAsync();


            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);

        }

        @Override
        public void onCancel() {
            Log.d("ff", "onCancle()");
        }

        @Override
        public void onError(FacebookException e) {
            Log.d("ff", "onEiooro()");
        }
    };

    public LoginFragment() {
        Log.d("ff", "login constructor yo()");
    }


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoginButton loginButton = (LoginButton) view.findViewById(R.id.login_button);
        textView = (TextView) view.findViewById(R.id.textView);

        loginButton.setReadPermissions("user_friends");
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, callback);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void displayMessage(Profile profile){
        if(profile != null){
            textView.setText(profile.getName());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }
}