package com.trnql.divamgupta.runwithfriends;

import com.trnql.smart.people.PersonEntry;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by divamgupta on 12/31/15.
 */

public class AllData {

    public static String name;

    public static String accessToken;
    public static String fbId;

    public  static String profileUrl;

    public static String myActivity; // from their api
    public static Double gps_x=0.0;
    public static Double gps_y=0.0;


    public static Double totalRun = 0.0;
    public static Double totalWalk = 0.0;



    public static HashMap<String, OtherPeople> local_friends = new HashMap<String, OtherPeople>();

    public static HashMap<String, OtherPeople> fb_friends = new HashMap<String, OtherPeople>();

    public static String toStringg() {
        return name + fbId + accessToken;
    }

    public static List<PersonEntry> io = new ArrayList<PersonEntry>();

    public static JSONArray Jsonarr = new JSONArray();
}
