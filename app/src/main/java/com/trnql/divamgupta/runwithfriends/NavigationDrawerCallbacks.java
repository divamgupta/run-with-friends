package com.trnql.divamgupta.runwithfriends;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
