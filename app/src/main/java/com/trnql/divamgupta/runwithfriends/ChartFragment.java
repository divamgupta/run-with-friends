package com.trnql.divamgupta.runwithfriends;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import java.io.InputStream;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChartFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChartFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChartFragment newInstance(String param1, String param2) {
        ChartFragment fragment = new ChartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ChartFragment() {
        // Required empty public constructor
    }


    String getHTML(String fName)
    {
        try{
            InputStream is = getActivity().getAssets().open(fName);
            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            return new String(buffer);


        } catch(Exception e){}

        return "hjhjh";


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chart, container, false);

        WebView webview = (WebView)view.findViewById(R.id.webView);
        webview.getSettings().setJavaScriptEnabled(true);
        String url = "file:///android_asset/a.html";

        String content = getHTML("b.html").replace("#1", AllData.totalRun.toString())  ;
//        System.out.print(content);
        webview.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "UTF-8", null);



        //Display list of friends:
//        String javascript="";
//        for(OtherPeople x:AllData.fb_friends.values())
//        {
//            //Print names of people
//            javascript+="javascript:document.write(\""+AllData.Jsonarr.toString() +"\");";
//        }
//        webview.loadUrl(javascript);
//        webview.loadUrl(url);
//        webview.loadUrl("javascript:doo(77)");

        return view;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
