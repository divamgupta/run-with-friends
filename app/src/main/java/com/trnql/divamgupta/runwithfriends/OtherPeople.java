package com.trnql.divamgupta.runwithfriends;

/**
 * Created by anshuman on 31/12/15.
 */
public class OtherPeople {
    public String name;
    public String mapStr;
    public double speed;
    public double latitude;
    public double longitude;
    public OtherPeople(){}
    public OtherPeople(String n,double lat,double lon)
    {
        this.name=n;
        this.latitude=lat;
        this.longitude=lon;
    }
}
